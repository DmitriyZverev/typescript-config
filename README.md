# @dmitriyzverev/typescript-config

_The [TypeScript](https://www.typescriptlang.org/) configuration that uses in my
packages_

---

## Usage

1. Install dependencies:

    ```bash
    npm i typescript @dmitriyzverev/typescript-config
    ```

2. Configure `tsconfig.json` file in the root of Your project:

    ```json
    {
        "extends": "@dmitriyzverev/typescript-config/tsconfig.json"
    }
    ```

3. Run [TypeScript](https://www.typescriptlang.org/) compiler:
    ```bash
    npx tsc
    ```

Also, You can extend specific server configuration:

```json
{
    "extends": "@dmitriyzverev/typescript-config/tsconfig.server.json"
}
```

```json
{
    "extends": "@dmitriyzverev/typescript-config/tsconfig.lib.server.json"
}
```

or client configuration:

```json
{
    "extends": "@dmitriyzverev/typescript-config/tsconfig.client.json"
}
```

```json
{
    "extends": "@dmitriyzverev/typescript-config/tsconfig.lib.client.json"
}
```
